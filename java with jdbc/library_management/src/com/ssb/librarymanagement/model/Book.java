package com.ssb.librarymanagement.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book {
    private int id;
    private String name;
    private String author_name;
    private String publisher_name;
    private String contact_number;
}
