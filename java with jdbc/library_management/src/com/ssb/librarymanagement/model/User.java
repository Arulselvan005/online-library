package com.ssb.librarymanagement.model;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class User {
    private int id;
    private int role_id;
    private String name;
    private String password;
    private String email_id;
    private String address;
    private String contact_number;
}