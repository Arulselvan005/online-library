package com.ssb.librarymanagement.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Role {
	private int id;
    private String name;
}
