package com.ssb.librarymanagement.controller;

import java.util.List;
import java.util.Scanner;

import com.ssb.librarymanagement.model.Book;
import com.ssb.librarymanagement.service.impl.BookServiceImpl;

public class BookController {
	//create singleton for book controller
	private static BookController bookController;
	public static BookController getInstance() {
		if(bookController == null) {
			bookController = new BookController();
		}
		return bookController;
	}
	 Scanner scanner = new Scanner(System.in);
	//add book
	public void add() {
    try {
    	//Add new book object for get book details
        Book book = getDetails();
        if(book!=null) {
          BookServiceImpl.getInstance().add(book);
        }
        System.out.println("Do you add another book y/ n");
        String input = scanner.nextLine();
        if((input).equals("y")) {
           add();
    }else if((input).equals("n")){
        System.out.println(" Thank you , Your Book is succesfully added");
    } else {
       System.out.println("Your Book is succesfully added");
       System.out.println("Please Enter the Valide Option : y or n");
    }
    }catch (Exception exception) {
      exception.printStackTrace();
      add();
    } 
  }
	/**
	 * @return book object
	 */
	public Book getDetails() {
		//Add new book object
        Book book = new Book();
        System.out.println("Enter the Book Name :");
        book.setName(scanner.nextLine());
        System.out.println("Enter the AuthorName :");
        book.setAuthor_name(scanner.nextLine());
        System.out.println("Enter the PublisherName :");
        book.setPublisher_name(scanner.nextLine());
        System.out.println("Enter the PublisherContactNumber :");
        book.setContact_number(scanner.nextLine());
        return book;
     }
  //view books
   public void books() {
	try {
		List<Book> book = BookServiceImpl.getInstance().book();
       System.out.println("----------------Books-----------------");
       System.out.println("ID \tBookName \tAuthor_Name \t Publisher_Name \tContact_Number ");
       System.out.println(" ");
       if(book.isEmpty()) {
           System.out.println("No Book");
       } else {
           for(Book iteratebook:book) {
                System.out.println(iteratebook.getId() + "\t" +iteratebook.getName() + "\t" + iteratebook.getAuthor_name() +"\t" + iteratebook.getPublisher_name()+"\t"+iteratebook.getContact_number());
           }
       }
	} catch (Exception e) {
		e.printStackTrace();
	}
   }
  /**
   * delete Book from BookList
   */
   public void delete() {
      try {
          books();
           List<Book> book = BookServiceImpl.getInstance().book();
            if(book.isEmpty()) {
               System.out.println("Book not is available ");
            } else {
                System.out.println("Enter Book ID :");
                int book_id = scanner.nextInt();
                BookServiceImpl.getInstance().delete(book_id);
                System.out.println( "succesfully deleted");
          }
        } catch(Exception exception) {
             exception.printStackTrace();
        }          
     }
}
