package com.ssb.librarymanagement.controller;

import java.util.List;
import java.util.Scanner;

import com.ssb.librarymanagement.model.Role;
import com.ssb.librarymanagement.model.User;
import com.ssb.librarymanagement.service.impl.UserServiceImpl;

public class UserController {
	//create singleton for User Controller
	private static UserController userController;
	   public static UserController getInstance(){
	       if(userController == null) {
	           userController = new UserController();
	        }
	        return userController;
	      }
	 Scanner scanner = new Scanner(System.in);
	 //Add user
	public void add() {
	       try {
	    	   //Add new user object for get user details
	           User user = getDetails();
//			   User valid = ValidationController.getInstance().validation(user);
//			   System.out.println(valid.getName());
//			   System.out.println(valid.getEmail_id());
//			   System.out.println(valid.getContact_number());
	           if (user!=null) {
	            UserServiceImpl.getInstance().add(user);
	           }
	         System.out.println("Do you add another User y/ n");
	          String input = scanner.nextLine();
	        if((input).equals("y")) {
	            add();
	       } else if((input).equals("n")){
	           System.out.println(" Thank you , User is succesfully added");
	       } else {
	          System.out.println("User is succesfully added");
	          System.out.println("Please Enter the Valide Option : y or n");
	         }
	       } catch(Exception exception) {
	           exception.printStackTrace();
	           add();
	       }
	   }
	/**
	 *  @return user object
	 */
	     public User getDetails() {
	    	 //Add new user object
	        User user = new User();
	     try {
	    	String role = "User";
		    Role roleDetails = UserServiceImpl.getInstance().getRole(role);
		    user.setRole_id(roleDetails.getId());
	        System.out.println("Enter the Name: ");
	        user.setName(scanner.nextLine());
	        System.out.println("Enter the Password:");
	        user.setPassword(scanner.nextLine());
	        System.out.println("Enter the Address:");
	        user.setAddress(scanner.nextLine());
	        System.out.println("Enter the Email Id:");
	        user.setEmail_id(scanner.nextLine());
	        System.out.println("Enter the Contact Number:");
	        user.setContact_number(scanner.nextLine());
	     } catch(Exception exception) {
	           exception.printStackTrace();
	           getDetails();
	     } 
	        return user;        
	    }
	/**
	 * list of User
	 */          
 public void users() {
		try {
			List<User> user = UserServiceImpl.getInstance().user();
	        System.out.println("---------------Users---------------");
	        System.out.println("ID \t Name \tAddress \tContact Number \tEmail Id ");
	        System.out.println(" ");
	        if(user.isEmpty()) {
	            System.out.println("No user");
	        } else {
	            for(User iterateuser:user) {
	                 System.out.println(iterateuser.getId() + "\t" +iterateuser.getName() + "\t" + iterateuser.getAddress() +"\t" + iterateuser.getContact_number()+"\t"+iterateuser.getEmail_id());
	            }
            }
		} catch (Exception e) {
				e.printStackTrace();
		}
}
	/**
	 * delete user from UserList
	 */
	   public void delete() {
	         try {
	           users();
	           List<User> user = UserServiceImpl.getInstance().user();
	             if(user.isEmpty()) {
	                 System.out.println("User not is available ");
	             } else {
	                  System.out.println("Enter User ID:");
	                  int user_id = scanner.nextInt();
	                  UserServiceImpl.getInstance().delete(user_id);
	                  System.out.println("succesfully deleted");
	            }
	         } catch (Exception exception) {
	              exception.printStackTrace();
	        } 
	   }
}
