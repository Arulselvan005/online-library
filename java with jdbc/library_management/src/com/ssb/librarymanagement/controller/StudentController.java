package com.ssb.librarymanagement.controller;

import java.util.List;
import java.util.Scanner;
import com.ssb.librarymanagement.controller.BookController;
import com.ssb.librarymanagement.controller.MainController;

@SuppressWarnings("unused")
public class StudentController {
	//create singleton for Student Controller
	private static StudentController studentController;
	   public static StudentController getInstance(){
	       if(studentController == null) {
	           studentController = new StudentController();
	        }
	        return studentController;
	      }
	    Scanner scanner = new Scanner(System.in);
	  //student initial process
	public void studentProcess() {
		 try {
	          System.out.println("Enter any one Option : \n1.ViewBook \t 2.Logout ");
				String studentOption = scanner.nextLine();
	            if((studentOption.equals("1"))||(studentOption.equals("2"))) {
	               int secondOption = Integer.parseInt(studentOption);
	              switch(secondOption) {
	                   case 1:
	                      BookController.getInstance().books();
	                   break;
	                   case 2:
	                      MainController.getInstance().initialProcess();
	                   break;
	                   default :
	                	   studentProcess();
	                	   break;
	               }
	            } else {
	                System.out.println("Please Enter the Numerical Type and Valide Option");
	                    studentProcess();
	           }
	           } catch (Exception exception) {
	               exception.printStackTrace();
	               studentProcess();
	           } 
	             studentProcess(); 
	      }
}
