package com.ssb.librarymanagement.controller;

import java.util.Scanner;
import com.ssb.librarymanagement.model.User;
import com.ssb.librarymanagement.service.impl.UserServiceImpl;

public class LoginController {
	//create singleton for login controller
	private static LoginController loginController;
	public static LoginController getInstance() {
		if(loginController == null) {
		 loginController = new LoginController();
		}
		return loginController;
	}
     Scanner scanner = new Scanner(System.in);
    public User login() {
    	//Add new user object
        User user = new User();
        System.out.println("\nEnter User name:");
        user.setName(scanner.nextLine());
        System.out.println("Enter password");
        user.setPassword(scanner.nextLine());
        return user;
    }
    //login process
    public void loginProcess() {
    	//Add new user object for login
    	User user =login();
		try {
			User login = UserServiceImpl.getInstance().loginValidation(user);
			if(login!= null) {
	            switch(login.getRole_id()) {
	            // user is Admin
	                case 1:
	                    System.out.println("Welcome Admin");
	                    AdminController.getInstance().adminProcess();
	                    break;
	                 // user is LibrarianAdmin
	                case 2:
	                    System.out.println("Welcome LibrarianAdmin");
	                    LibrarianAdminController.getInstance().librarianAdminProcess();
	                    break;
	                 // user is  student   
	                case 3:
	                	System.out.println("Welcome User");
	                	StudentController.getInstance().studentProcess();
	                	break;
	                default :
	                	loginProcess();
	                	break;
	            }
	        }
	        else {
	            System.out.println("Username or Password is invalid");
	            loginProcess();
	        }
		} catch (Exception exception) {
			//exception.printStackTrace();
			System.out.println("Username or Password is invalid");
			loginProcess();
		}
    }
}