/**
 * <p>
 * Library Management System Project
 * </p>
 * @author Arulselvan created 15/9/2020
 */
package com.ssb.librarymanagement.controller;


import java.util.Scanner;
import com.ssb.librarymanagement.controller.LoginController;
import com.ssb.librarymanagement.controller.RegisterController;


@SuppressWarnings("unused")
public class MainController {
	//create singleton for Main Controller
	public static MainController mainController;
    public static MainController getInstance(){
     if(mainController == null) {
         mainController = new MainController();
      }
      return mainController;
    }
     Scanner scanner = new Scanner(System.in);
    //Initial Process 
    public void initialProcess() {
    try {
       System.out.println("Enter any one \n 1.Login \t 2.Register");
          String initialProcessOption = scanner.nextLine();
          if((initialProcessOption.equals("1"))||(initialProcessOption.equals("2"))) {
           int initialProcess = Integer.parseInt(initialProcessOption);
              switch(initialProcess) {
                    case 1:
                         LoginController.getInstance().loginProcess();
                         break;
                    case 2:
                         RegisterController.getInstance().registerForm();
                         break;
                    default :
                        initialProcess();
                        break;
             }
         } else {
            System.out.println("Please Enter the Numerical Type and Valide Option 1 or 2");
               initialProcess();
        }
      }catch (Exception exception) {
          exception.printStackTrace();
      } 
           initialProcess();
  }
    //create Main method for Library Management project
    public static void main(String args[]) {
        MainController mainController = new MainController ();
        mainController.initialProcess();
    }
}
