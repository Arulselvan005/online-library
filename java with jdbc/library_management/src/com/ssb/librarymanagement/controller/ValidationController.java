package com.ssb.librarymanagement.controller;

import com.ssb.librarymanagement.model.User;
import com.ssb.librarymanagement.service.impl.UserServiceImpl;


public class ValidationController {
	private static ValidationController validationController;
	public static ValidationController getInstance() {
		if(validationController == null) {
			validationController = new ValidationController();
			return validationController;
		}
		return validationController;	
	}
    public User validation(User user) {
	try {
		User userValid = UserServiceImpl.getInstance().validation(user);
		 if(userValid != null) {
		 	 UserController.getInstance().getDetails();
		   }
		 return userValid;
	} catch (Exception e) {
		e.printStackTrace();
	}
	return user;	
 }
}
