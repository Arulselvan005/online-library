package com.ssb.librarymanagement.controller;

import java.util.Scanner;
import com.ssb.librarymanagement.controller.BookController;
import com.ssb.librarymanagement.controller.MainController;

@SuppressWarnings("unused")
public class LibrarianAdminController {
	//create singleton for LibrarianAdmin Controller
	private static LibrarianAdminController librarianAdminController;
    public static LibrarianAdminController getInstance(){
       if(librarianAdminController == null) {
           librarianAdminController = new LibrarianAdminController();
        }
        return librarianAdminController;
      }
     Scanner scanner = new Scanner(System.in);
   //librarianAdmin initial process
    public void librarianAdminProcess() {
      try {
        System.out.println("Enter any one option :\n1.Add Book \t 2.Book List \t 3.delete Book \t 4.Logout");
             String librarianAdminOption = scanner.nextLine();
           if((librarianAdminOption.equals("1"))||(librarianAdminOption.equals("2"))||(librarianAdminOption.equals("3"))||(librarianAdminOption.equals("4"))) {
               int secondLibrarianAdminOption = Integer.parseInt(librarianAdminOption);
             switch(secondLibrarianAdminOption) {
                  case 1:
                      BookController.getInstance().add();
                      break;
                  case 2:
                      BookController.getInstance().books();
                      break;
                  case 3:
                       BookController.getInstance().delete();                         
                       break;
                  default:
                       MainController.getInstance().initialProcess();
                       break;
            }
          }else {
              System.out.println("Please Enter the Numerical Type and Valide Option");
                      librarianAdminProcess();
          }
         }catch (Exception exception) {
            exception.printStackTrace();
            librarianAdminProcess();
         } 
      librarianAdminProcess();
   }
}
