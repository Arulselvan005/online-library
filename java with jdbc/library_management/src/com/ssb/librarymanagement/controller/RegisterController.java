package com.ssb.librarymanagement.controller;

import com.ssb.librarymanagement.model.User;
import com.ssb.librarymanagement.service.impl.UserServiceImpl;

public class RegisterController {
	//create singleton for Register Controller
    private static RegisterController registerController;
    public static RegisterController getInstance() {
    	if(registerController == null) {
    		registerController = new RegisterController();
    	}
		return registerController;
    }
    //register process
    public void registerForm() {
    	//Add new user object for get user details
    	User user = UserController.getInstance().getDetails();     
    	if(user!= null) {
            try {
				UserServiceImpl.getInstance().add(user);
			} catch (Exception exception) {
				exception.printStackTrace();
				registerForm();
			}
            System.out.println(" Registered Successfully ");
        }
       else {
           System.out.println("Register is invalid");
           registerForm();
       }
  }
}
