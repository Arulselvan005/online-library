package com.ssb.librarymanagement.service;

import java.util.List;

import com.ssb.librarymanagement.model.Book;

public interface BookService {
	 boolean add(Book book) throws Exception;
	 List<Book> book() throws Exception;
	 boolean delete(int book_id) throws Exception;
}
