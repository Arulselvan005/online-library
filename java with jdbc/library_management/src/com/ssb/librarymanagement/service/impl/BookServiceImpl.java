package com.ssb.librarymanagement.service.impl;

import java.util.List;

import com.ssb.librarymanagement.dao.impl.BookDaoImpl;
import com.ssb.librarymanagement.model.Book;
import com.ssb.librarymanagement.service.BookService;

public class BookServiceImpl implements BookService {
	 //create singleton for bookService
	private static BookService bookService;
	public static BookService getInstance() {
		if(bookService == null) {
			bookService = new BookServiceImpl();
		}
		return bookService;
	}
	/**
	  * add book in list
	  * @param book object
	  * @return boolean value
	  * @throws Exception 
	  */
	 @Override
	public boolean add(Book book) throws Exception {
		 try {
		    return BookDaoImpl.getInstance().add(book);
		 } catch(Exception exception) {
			 throw new Exception(exception);
		 }
    }
	 /**
	  * List of Book
	  * @return boolean value
	  * @throws Exception 
	  */
	 @Override
    public List<Book> book() throws Exception {
		 try {
		 return BookDaoImpl.getInstance().book();
		 } catch(Exception exception) {
			 throw new Exception(exception);
		 }
    }
	 /**
	  * delete book
	  * @param bookId
	  * @return boolean value
	  * @throws Exception 
	  */
	 @Override
    public boolean delete(int book_id) throws Exception {
		 try {
			 return BookDaoImpl.getInstance().delete(book_id);
		 } catch(Exception exception) {
			 throw new Exception(exception);
		 }
    }
}
