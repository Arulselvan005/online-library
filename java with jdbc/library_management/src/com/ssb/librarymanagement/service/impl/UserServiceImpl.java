package com.ssb.librarymanagement.service.impl;

import java.util.List;

import com.ssb.librarymanagement.service.UserService;
import com.ssb.librarymanagement.model.Role;
import com.ssb.librarymanagement.dao.impl.UserDaoImpl;
import com.ssb.librarymanagement.model.User;

public class UserServiceImpl implements UserService {
	//create singleton for userService
	private static UserService userService;
	public static UserService getInstance() {
		if(userService == null) {
			userService = new UserServiceImpl();
		}
		return userService;	
	  }
	/**
     * login validation
     * @param user object
     * @return boolean value
	 * @throws Exception 
     */
	 public User loginValidation(User user) throws Exception {      
			try {
				User login = UserDaoImpl.getInstance().getDetails(user);
				if((login.getName().equals(user.getName()))) {
				     if((login.getPassword().equals(user.getPassword()))) {
			             return login;    
				     }else {
				    	 return null;
				     }
				} else {
					return null;
				}
			} catch (Exception exception) {
				throw new Exception(exception);
			}	
	 }
	 /**
	  * add User 
	  * @param user object
	  * @return boolean value
	  * @throws Exception 
	  */
	 @Override
	 public boolean add(User user) throws Exception {
		 try {
			 return UserDaoImpl.getInstance().add(user);
		 } catch(Exception exception) {
			 throw new Exception(exception);
		 }
	 }
	 /**
	  * List of User
	  * @return boolean value
	  * @throws Exception 
	  */
	 @Override
	 public List<User> user() throws Exception {
		 try {
			 return UserDaoImpl.getInstance().user();	        
		 } catch(Exception exception) {
			 throw new Exception(exception);
		 }
	        
	 }
	 /**
	  * delete user 
	  * @param userId
	  * @return boolean value
	  * @throws Exception 
	  */
	 @Override
	 public boolean delete(int user_id) throws Exception {
		 try {
			 return UserDaoImpl.getInstance().delete(user_id);
		 } catch(Exception exception) {
			 throw new Exception(exception);
		 }
	 }
	 /**
	  * get role 
	  * @param user object
	  * @return role object
	  * @throws Exception 
	  */
	 @Override
	 public Role getRole(String user) throws Exception {
		 try {
			 return UserDaoImpl.getInstance().getRole(user);
		 } catch(Exception exception) {
			 throw new Exception(exception);
		 }
	 }
	 public User validation(User user) throws Exception {
		 try {
			User valid = UserDaoImpl.getInstance().getValidationDatails(user);
			if((valid.getName()).equals(user.getName())) {
				System.out.println("This Name is Already registered. Try to another Name");
				return valid;
			}
			else if((valid.getEmail_id()).equals(user.getEmail_id())) {
				System.out.println("This EmailId is Already registered. Try to another EmailId");
				return valid;
			}
			else if((valid.getContact_number()).equals(user.getContact_number())) {
				System.out.println("This ContactNumber is Already registered. Try to another Number");
				return valid;
			}
			return null;
		 } catch(Exception exception) {
			 throw new Exception(exception);
		 }
		}
}
