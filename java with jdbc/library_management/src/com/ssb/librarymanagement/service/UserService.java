package com.ssb.librarymanagement.service;

import java.util.List;

import com.ssb.librarymanagement.model.Role;
import com.ssb.librarymanagement.model.User;

public interface UserService {
	 User loginValidation(User user) throws Exception;
	 boolean add(User user) throws Exception;
	 List<User> user() throws Exception;
	 boolean delete(int user_id) throws Exception;
	 Role getRole(String User) throws Exception;
	 User validation(User User) throws Exception;
}
