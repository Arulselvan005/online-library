package com.ssb.librarymanagement.exception;

public class AddUserException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public AddUserException(AddUserException exception) {
		super(exception);
	}

}
