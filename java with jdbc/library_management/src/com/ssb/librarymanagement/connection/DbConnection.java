package com.ssb.librarymanagement.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
    private static String url = "jdbc:mysql://localhost/library_management";
    private static String driverName = "com.mysql.jdbc.Driver";
    private static String username = "root";
    private static String password = "root";
    private static Connection con;

    public static Connection getConnection() {
    	//create connection from database
        try {
            Class.forName(driverName);
            try {
                con = DriverManager.getConnection(url, username, password);
            } catch (SQLException exception) {
                // database connection an exception
            	exception.printStackTrace();
                System.out.println("Failed to create the database connection."); 
            }
        } catch (ClassNotFoundException exception) {
            // driver not found an exception
            System.out.println("Driver not found."); 
        }
        return con;
    }
    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } 
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}


