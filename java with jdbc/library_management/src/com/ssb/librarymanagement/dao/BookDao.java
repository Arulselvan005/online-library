package com.ssb.librarymanagement.dao;

import java.util.List;

import com.ssb.librarymanagement.model.Book;

public interface BookDao {
	 List<Book> book() throws Exception;
	 boolean delete(int book_id) throws Exception;
	 boolean add(Book book) throws Exception;
}
