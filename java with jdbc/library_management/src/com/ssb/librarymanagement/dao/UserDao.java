package com.ssb.librarymanagement.dao;

import java.util.List;

import com.ssb.librarymanagement.exception.AddUserException;
import com.ssb.librarymanagement.model.Role;
import com.ssb.librarymanagement.model.User;

public interface UserDao {
	 List<User> user() throws Exception;
	 boolean add(User user) throws Exception,AddUserException;
	 boolean delete(int user_id) throws Exception;
	 User getDetails(User user) throws Exception;
	 Role getRole(String User) throws Exception;
	 User getValidationDatails(User user) throws Exception;
}