package com.ssb.librarymanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ssb.librarymanagement.dao.UserDao;
import com.ssb.librarymanagement.exception.AddUserException;
import com.ssb.librarymanagement.model.User;
import com.ssb.librarymanagement.model.Role;
import com.ssb.librarymanagement.connection.DbConnection;

public class UserDaoImpl implements UserDao {
	//create singleton for userDao
	private static UserDao userDao;
	public static UserDao getInstance() {
		if(userDao == null) {
			userDao = new UserDaoImpl();
		}
		return userDao;
	}
	//save users
	@Override
	public boolean add(User user)throws Exception, AddUserException {
		String sql = "INSERT INTO user(role_id,name,password,email_id,address,contact_number) VALUES (?,?,?,?,?,?)";
		Connection con = DbConnection.getConnection();
	    boolean insertStatus = true;
		try{
			PreparedStatement preparedStatement = con.prepareStatement(sql);
		    preparedStatement.setInt(1,user.getRole_id());
	        preparedStatement.setString(2,user.getName());
	        preparedStatement.setString(3,user.getPassword());
	        preparedStatement.setString(4, user.getEmail_id());
	        preparedStatement.setString(5, user.getAddress());
	        preparedStatement.setString(6, user.getContact_number());
	        insertStatus = preparedStatement.execute();
	        } catch(SQLException exception) {
	            throw new Exception(exception);
//	        } catch(AddUserException exception) {
//	        	throw new AddUserException(exception);
	        } finally {
	        	DbConnection.closeConnection(con);
	        }     
	           return insertStatus;
	}
	/**
	 * retrieve user
	 * @return user
	 * @throws Exception 
	 */
	@Override
    public List<User> user()throws Exception {
        List<User> user = new ArrayList<User>();
        String sql = "SELECT * FROM user";
        Connection con = DbConnection.getConnection();
        try {
        	PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery(sql);
            while (result.next()) {
                User userDetails = new User();
                userDetails.setId(result.getInt("id"));
                userDetails.setRole_id(result.getInt("role_id"));
                userDetails.setName(result.getString("name"));
                userDetails.setPassword(result.getString("password"));
                userDetails.setEmail_id(result.getString("email_id"));
                userDetails.setAddress(result.getString("address"));
                userDetails.setContact_number(result.getString("contact_number"));
                user.add(userDetails);
            }
        } catch(SQLException exception) {
           throw new Exception(exception);
        } 
        return user;
    }
	/**
     * @param user_id
     * @return boolean 
     * @throws Exception 
     */
	@Override
	public boolean delete(int user_id)throws Exception {
        boolean deleteStatus = true;
        String sql = "delete FROM user where id = ?";
        Connection con = DbConnection.getConnection();
        try {
        	PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, user_id);
            deleteStatus = preparedStatement.execute();
        } catch(SQLException exception) {
            throw new Exception(exception);
        } finally {
        	DbConnection.closeConnection(con);
        } 
        return deleteStatus;
    }
	/**
     * @param user object 
     * @return loginUser object
     * @throws Exception
     */
	@Override
	public User getDetails(User user)throws Exception {
        User login = new User();
        String sql = "SELECT * FROM user WHERE NAME = '"+ user.getName()+"'";
        Connection con = DbConnection.getConnection();
        try {
        	PreparedStatement preparedstatement = con.prepareStatement(sql);
            ResultSet result = preparedstatement.executeQuery(sql);
            while (result.next()) {
                login.setId(result.getInt("id"));
                login.setName(result.getString("name"));
                login.setPassword(result.getString("password"));
                login.setRole_id(result.getInt("role_id"));
            }
        } catch(SQLException exception) {
        	throw new Exception(exception);
        } finally {
        	 DbConnection.closeConnection(con);	
        }
        return login;
    }
	/**
	 * @param user object
	 * @return role object
	 * @throws Exception  
	 */
	@Override
	public Role getRole(String user)throws Exception {
        Role role = new Role();
        String sql = "SELECT * FROM role WHERE NAME = '"+user+"';";
        Connection con = DbConnection.getConnection();
        try {
        	PreparedStatement preparedstatement = con.prepareStatement(sql);
            ResultSet result = preparedstatement.executeQuery(sql);
            while (result.next()) {
                role.setId(result.getInt("id"));
                role.setName(result.getString("name"));
            }
        } catch(SQLException exception) {
        	throw new Exception(exception);
        } finally {
        	DbConnection.closeConnection(con);
        }
        return role;
    }
	@Override
	public User getValidationDatails(User user)throws Exception {
		 User valid = new User();
	        String sql = "SELECT * FROM user WHERE NAME = '"+ user.getName()+"'";
	        Connection con = DbConnection.getConnection();
	        try {
	        	PreparedStatement preparedstatement = con.prepareStatement(sql);
	            ResultSet result = preparedstatement.executeQuery(sql);
	            while (result.next()) {
	                valid.setId(result.getInt("id"));
	                valid.setName(result.getString("name"));
	                valid.setPassword(result.getString("password"));
	                valid.setRole_id(result.getInt("role_id"));
	                valid.setAddress(result.getString("address"));
	                valid.setEmail_id(result.getString("email_id"));
	                valid.setContact_number(result.getString("contact_number"));
	            }
	        } catch(SQLException exception) {
	        	throw new Exception(exception);
	        } finally {
	        	DbConnection.closeConnection(con);	
	        }
	        return valid;
	}
}
