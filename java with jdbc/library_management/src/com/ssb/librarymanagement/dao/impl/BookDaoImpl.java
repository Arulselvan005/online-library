package com.ssb.librarymanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ssb.librarymanagement.connection.DbConnection;
import com.ssb.librarymanagement.dao.BookDao;
import com.ssb.librarymanagement.model.Book;

public class BookDaoImpl implements BookDao {
	//create singleton for bookDao
	private static BookDao bookDao;
	public static BookDao getInstance( ) {
		if(bookDao == null) {
			bookDao = new BookDaoImpl();
		}
		return bookDao;	
	}
	/**
	 * save book
	 * @param book object
	 * @return boolean 
	 * @throws Exception 
	 */
	@Override
	public boolean add(Book book)throws Exception {
		String sql = "INSERT INTO book(name,author_name,publisher_name,contact_number) VALUES (?,?,?,?)";
		Connection con = DbConnection.getConnection();
		    boolean insertStatus = true;
			try{
				PreparedStatement preparedStatement = con.prepareStatement(sql);
			    preparedStatement.setString(1,book.getName());
		        preparedStatement.setString(2,book.getAuthor_name());
		        preparedStatement.setString(3,book.getPublisher_name());
		        preparedStatement.setString(4,book.getContact_number());
		        insertStatus = preparedStatement.execute();
		        } catch(SQLException exception) {
		            throw new Exception(exception);
		        } finally {
		        	DbConnection.closeConnection(con);	
		        }
		           return insertStatus;
		}
	  /**
	   * retrieve book
	   * @return bookList
	   * @throws Exception 
	   */
		@Override
	    public List<Book> book() throws Exception {
	        List<Book> book = new ArrayList<Book>();
	        String sql = "SELECT * FROM book";
	        Connection con = DbConnection.getConnection();
	        try {
	        	PreparedStatement preparedStatement = con.prepareStatement(sql);
	            ResultSet result = preparedStatement.executeQuery(sql);
	            while (result.next()) {
	                Book bookDetails = new Book();
	                bookDetails.setId(result.getInt("id"));
	    			bookDetails.setName(result.getString("name"));
	    			bookDetails.setAuthor_name(result.getString("author_name"));
	                bookDetails.setPublisher_name(result.getString("publisher_name"));
	                bookDetails.setContact_number(result.getString("contact_number"));
	                book.add(bookDetails);
	            }
	        } catch(SQLException exception) {
	            throw new Exception(exception);
	        }
	        return book;
	    }
		/**
	     * @param book_id
	     * @return boolean 
	     * @throws Exception 
	     */
		@Override
		public boolean delete(int book_id)throws Exception {
	        boolean deleteStatus = true;
	        String sql = "delete FROM book where id = ?";
	        Connection con = DbConnection.getConnection();
	        try {
	        	PreparedStatement preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, book_id);
	            deleteStatus = preparedStatement.execute();
	        } catch(SQLException exception) {
	            throw new Exception(exception);
	        } finally {
	        	 DbConnection.closeConnection(con);	
	        }
	        return deleteStatus;
	    }
	}
