
package com.ssb.librarymanagement.service;

import java.util.Map;



import java.util.List;

import com.ssb.librarymanagement.models.User;

public interface UserService {

	void addUser(User user);

	List<User> userList();

        void deleteUser(String user);
        void userCheck(String name);
        void studentCheck(String name1);
        public boolean loginValidation(Map map, User user);
     }