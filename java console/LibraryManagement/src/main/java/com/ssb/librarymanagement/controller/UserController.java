package com.ssb.librarymanagement.controller;


import java.util.List;
import java.util.Scanner;
import com.ssb.librarymanagement.models.Book;
import com.ssb.librarymanagement.models.User;
import com.ssb.librarymanagement.service.implementation.UserServiceImplementation;
import com.ssb.librarymanagement.service.implementation.BookServiceImplementation;
import com.ssb.librarymanagement.LibraryManagement;
import com.ssb.librarymanagement.controller.AdminController;

public class UserController {

   public static UserController userController;
   public static UserController getInstance(){
       if(userController == null) {
          UserController userController = new UserController();
           return userController;
        }
        return userController;
      }

   private static Scanner scanner = new Scanner(System.in);

      public void userProcess() {
         
         try {
          System.out.println("Enter any one Option : \n1.ViewBook \t 2.Logout ");
             String number = scanner.nextLine();
            if((number.equals("1"))||(number.equals("2"))) {
               int number1 = Integer.parseInt(number);
              switch(number1) {
                   case 1:
                      bookList();
                   break;
                   case 2:
                      LibraryManagement.getInstance().registerProcess();
                   break;
               }
            } else {
                System.out.println("Please Enter the Numerical Type and Valide Option");
                    userProcess();
           }
           } catch (java.lang.NullPointerException exception) {
               //exception.printStackTrace();
           } finally {
              //scanner.close();
             userProcess();
           } 
      }

   public void bookList(){
        try {
          List<Book> book = BookServiceImplementation.getInstance().bookList();
          System.out.println("Books");

          System.out.println("Id\t BookName \tAuthorName \t PublisherName \tPublisherContactNumber ");

          System.out.println(" ");

             if(book.isEmpty()) {

                 System.out.println("No Book");

             } else {

                for(Book iteratebook:book) {

                   System.out.println(iteratebook.getName() + "\t" + iteratebook.getAuthorName() +"\t" + iteratebook.getPublisherName()+"\t"+iteratebook.getPublisherContactNumber());

                }

            }
          }catch(java.lang.NullPointerException exception) {
              //exception.printStackTrace();
          } finally {
          userProcess();
         }
      }

      public User registerForm() {
           User user = getUserDetails();
        try {
 
         if(user!= null) {

          UserServiceImplementation.getInstance().addUser(user);

      }

      }catch(java.lang.NullPointerException exception) {
        //exception.printStackTrace();
      }
       return user;
 

     }

     public User getUserDetails() {
        User user = new User();
        
     try {
        System.out.println("Enter the Name: ");
        String name1 = scanner.nextLine();
        UserServiceImplementation.getInstance().studentCheck(name1);
        user.setName(name1);
        System.out.println("enter the role :");
        user.setRole(scanner.nextLine());
        System.out.println("Enter the Password:");
        user.setPassword(scanner.nextLine());
        System.out.println("Enter the Address:");
        user.setAddress(scanner.nextLine());
        System.out.println("Enter the Email Id:");
        user.setEmailId(scanner.nextLine());
        System.out.println("Enter the Contact Number:");
        user.setContactNumber(scanner.nextLine());
     } catch(java.lang.NullPointerException exception) {
           //exception.printStackTrace();
     } finally {
          //scanner.close();
     }
        return user;        
    }          
}
