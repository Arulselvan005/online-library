/**
 * @author S.Arulselvan  
 * Date 1/7/2020.
 */

package com.ssb.librarymanagement;


import java.util.HashMap;

import java.util.Map;
import java.util.Scanner;

import com.ssb.librarymanagement.service.implementation.UserServiceImplementation;

import com.ssb.librarymanagement.controller.AdminController;

import com.ssb.librarymanagement.controller.LibrarianAdminController;
import com.ssb.librarymanagement.controller.UserController;
import com.ssb.librarymanagement.models.User;

public class LibraryManagement {

      public static LibraryManagement libraryManagement;
      public static LibraryManagement getInstance(){
       if(libraryManagement == null) {
          LibraryManagement libraryManagement = new LibraryManagement();
           return libraryManagement;
        }
        return libraryManagement;
      }

     public static Map<String,String> usermap = new HashMap<String,String>();
    private static Scanner scanner = new Scanner(System.in);

    public void registerProcess() {
           
       try {
          System.out.println("Enter any one \n 1.Login \t 2.New Student");
             String number =scanner.nextLine();
             if((number.equals("1"))||(number.equals("2"))) {
              int number1 = Integer.parseInt(number);
                 switch(number1) {
                       case 1:
                            loginProcess();
                            break;
                       case 2:
                           User user = UserController.getInstance().registerForm();
                           usermap.put(user.getName() , user.getPassword());
                           registerProcess();
                           break;
                }
            } else {
               System.out.println("Please Enter the Numerical Type and Valide Option 1 or 2");
                  registerProcess();
           }
         }catch (java.lang.NullPointerException exception) {
             //exception.printStackTrace();
         }  finally {
             //scanner.close();
              registerProcess();
         }
     }

    public void loginProcess() {
          User user = login();         
             boolean status = UserServiceImplementation.getInstance().loginValidation(usermap, user);
          try {
                if(status) {

                     switch(user.getName()){
                         case "Admin":

                            AdminController.getInstance().adminProcess();

                            break;
			 case "LibrarianAdmin":
			    LibrarianAdminController.getInstance().librarianAdminProcess();
			    break;
                          default:
                            UserController.getInstance().userProcess();
                            break;
                       }
                  } else{

                       System.out.println("username or password is wrong");
                            registerProcess();

                  }
        } catch (java.lang.NullPointerException exception) {
            //exception.printStackTrace();
        } finally {

                registerProcess();
        }
    } 

    public User login() {

                User user = new User();
        
      try {
        System.out.println("\nEnter User name:");

        user.setName(scanner.next());

        System.out.println("Enter password");

        user.setPassword(scanner.next());
      } catch (Exception exception) {
        //exception.printStackTrace();
      } finally {
          //scanner.close();
      }
          return user;

    }

  

   
 
    public static void main(String args[]) {

         usermap.put("Admin","Admin");
         usermap.put("LibrarianAdmin","LibrarianAdmin");
        LibraryManagement libraryManagement = new LibraryManagement ();

        libraryManagement.registerProcess();



    }


}

   


