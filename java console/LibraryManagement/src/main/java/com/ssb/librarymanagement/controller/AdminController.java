package com.ssb.librarymanagement.controller;





import java.util.List;

import java.util.Scanner;
import com.ssb.librarymanagement.models.User;
import com.ssb.librarymanagement.models.Book;
import com.ssb.librarymanagement.service.implementation.BookServiceImplementation;
import com.ssb.librarymanagement.service.implementation.UserServiceImplementation;
import com.ssb.librarymanagement.LibraryManagement;
import com.ssb.librarymanagement.controller.LibrarianAdminController;
import com.ssb.librarymanagement.controller.UserController;


public class AdminController {

    public static AdminController adminController;
    public static AdminController getInstance(){
       if(adminController == null) {
          AdminController adminController = new AdminController();
           return adminController;
        }
        return adminController;
      }

    private static Scanner scanner = new Scanner (System.in);

    
    public void adminProcess() {
     
            try {
                System.out.println(" Enter any one option :\n1.Members \t 2. Books \t 3.Logout ");

                   String adminOption = scanner.nextLine();

                  if((adminOption.equals("1"))||(adminOption.equals("2"))||(adminOption.equals("3"))) {
                    int adminOption1 = Integer.parseInt(adminOption);
                   switch(adminOption1) {

                       case 1:
                           System.out.println("Enter any one option :\n1.Add User \t 2.User List \t 3.delete User \t 4.Previous ");
                              adminOption = scanner.nextLine();

                             if((adminOption.equals("1"))||(adminOption.equals("2"))||(adminOption.equals("3"))||(adminOption.equals("4"))) {
                                   adminOption1 = Integer.parseInt(adminOption);
                                switch(adminOption1) {

                   		    case 1:

                         	       addUser();
                           	    break;

                                    case 2:

                                       userList();

                                    break;

                                    case 3:

                                        deleteUser();

                                    break;

                                    case 4:
                                         adminProcess();
                                    break;
                                }
                                } else {
                                    System.out.println("Please Enter the Numerical Type and Valide Option ");
                                                 adminProcess();
                                } 
                                     
                         break;
                        case 2:
                            System.out.println("Enter any one option :\n1.Add Book \t 2.Book List \t 3.delete Book \t4.Previous");
                                adminOption = scanner.nextLine();

                                if((adminOption.equals("1"))||(adminOption.equals("2"))||(adminOption.equals("3"))||(adminOption.equals("4"))) {
                                   adminOption1 = Integer.parseInt(adminOption);
                                    switch(adminOption1) {

                                         case 1:

                                            LibrarianAdminController.getInstance().addBook();
                                            adminProcess();
                                            break;

                                        case 2:

                                            LibrarianAdminController.getInstance().bookList();
                                            adminProcess();
                                            break;

                                        case 3:
                                           LibrarianAdminController.getInstance().deleteBook();
                                           adminProcess();
                                           break;

                                        case 4:

                                           adminProcess();

                                           break;
                                   }
                                   } else {
                                    System.out.println("Please Enter the Numerical Type and Valide Option");
                                           adminProcess();
                                } 
                            break;

                           case 3:
                                  LibraryManagement.getInstance().registerProcess();
                                break;
                       } 
                      }else {
                            System.out.println("Please Enter the Numerical Type and Valide Option");

                                          adminProcess();
                   } 
                } catch (java.lang.NullPointerException exception) {
                     //exception.printStackTrace();
                } finally {
                   //scanner.close();
	          adminProcess();
               }
       }

    public void addUser() {
               try {
           User user = getUserDetails();
         if(user!= null) {

            UserServiceImplementation.getInstance().addUser(user);

        }

        System.out.println("Do you add another User y/ n");

          String input = scanner.nextLine();
        if((input).equals("y")) {

            addUser();

       } else if((input).equals("n")){
           System.out.println(" Thank you , User is succesfully added");
       } else {
          System.out.println("User is succesfully added");
          System.out.println("Please Enter the Valide Option : y or n");
               adminProcess();
       }
     } catch(java.lang.NullPointerException exception) {
       //exception.printStackTrace();
     } finally {
        //scanner.close();
     }
    }

     public User getUserDetails() {
        User user = new User();
        
     try {
        System.out.println("Enter the Name: ");
        String name = scanner.nextLine();
        UserServiceImplementation.getInstance().userCheck(name);
        user.setName(name);
        System.out.println("enter the role :");
        user.setRole(scanner.nextLine());
        System.out.println("Enter the Password:");
        user.setPassword(scanner.nextLine());
        System.out.println("Enter the Address:");
        user.setAddress(scanner.nextLine());
        System.out.println("Enter the Email Id:");
        user.setEmailId(scanner.nextLine());
        System.out.println("Enter the Contact Number:");
        user.setContactNumber(scanner.nextLine());
     } catch(java.lang.NullPointerException exception) {
           //exception.printStackTrace();
     } finally {
          //scanner.close();
     }
        return user;        
    }
         
    public void userList() {

        List<User> user = UserServiceImplementation.getInstance().userList();

        System.out.println("User");

        System.out.println("Name \tAddress \tContactNumber \tEmailId ");

        System.out.println(" ");

        if(user.isEmpty()) {

            System.out.println("No user");

        } else {

            for(User iterateuser:user) {

                 System.out.println(iterateuser.getName() + "\t" + iterateuser.getAddress() +"\t" + iterateuser.getContactNumber()+"\t"+iterateuser.getEmailId());

        }

     }

   }


   public void deleteUser() {
       //Scanner scanner = null;
         //scanner = new Scanner(System.in);
         try {
           userList();
           List<User> user = UserServiceImplementation.getInstance().userList();

             if(user.isEmpty()) {

                 System.out.println("User not is available ");

             } else {

                  System.out.println("Enter User Name:");
                  String userName = scanner.nextLine();
                  UserServiceImplementation.getInstance().deleteUser(userName);

                 System.out.println("succesfully deleted");

            }
         } catch (java.lang.NullPointerException exception) {
              //exception.printStackTrace();
        } finally {
            //scanner.close();
        }
    }
}
