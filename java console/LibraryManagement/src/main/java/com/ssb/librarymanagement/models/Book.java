package com.ssb.librarymanagement.models;

public class Book {


    private int id;

    private String name;
    private String authorName;

    private String publisherName;
    private String publisherContactNumber;

 

   public int getId() {

        return id;

    }

     public void setId(int id) {

        this.id = id;

    }
    public String getName() {

        return name;

    }



    public void setName(String name) {

        this.name = name;

    }

    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
         this.authorName = authorName;
    }
    public String getPublisherName() {
         return publisherName;
    }
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }
    public String getPublisherContactNumber() {
        return publisherContactNumber;
    }
    public void setPublisherContactNumber(String publisherContactNumber) {
        this.publisherContactNumber = publisherContactNumber;
   }
}
