package com.ssb.librarymanagement.controller;


import java.util.Scanner;
import java.util.List; 
import com.ssb.librarymanagement.models.Book;
import com.ssb.librarymanagement.service.implementation.BookServiceImplementation;
import com.ssb.librarymanagement.LibraryManagement;

public class LibrarianAdminController {

    public static LibrarianAdminController librarianAdminController;
    public static LibrarianAdminController getInstance(){
       if(librarianAdminController == null) {
          LibrarianAdminController librarianAdminController = new LibrarianAdminController();
           return librarianAdminController;
        }
        return librarianAdminController;
      }

    private static Scanner scanner = new Scanner(System.in);

      public void librarianAdminProcess() {
          
        try {
          System.out.println("Enter any one option :\n1.Add Book \t 2.Book List \t 3.delete Book \t 4.Logout");
               String librarianAdminOption = scanner.nextLine();

             if((librarianAdminOption.equals("1"))||(librarianAdminOption.equals("2"))||(librarianAdminOption.equals("3"))||(librarianAdminOption.equals("4"))) {
                 int librarianAdminOption1 = Integer.parseInt(librarianAdminOption);
               switch(librarianAdminOption1) {

                    case 1:

                        addBook();

                        break;

                    case 2:

                        bookList();

                        break;

                    case 3:
                         deleteBook();                         
                         break;

                    case 4:
                         LibraryManagement.getInstance().registerProcess();
                         break;
              }
            }else {
                System.out.println("Please Enter the Numerical Type and Valide Option");

                        librarianAdminProcess();
            }
           }catch (java.lang.NullPointerException exception) {
              //exception.printStackTrace();
           } finally {
               //scanner.close();
              librarianAdminProcess();
           } 
     }

     public void addBook() {
          
      try {
         Book book = getBookDetails();
         if(book!=null) {
             BookServiceImplementation.getInstance().addBook(book);
         }
         System.out.println("Do you add another book y/ n");
          String input = scanner.nextLine();
         if((input).equals("y")) {

            addBook();

       }else if((input).equals("n")){
           System.out.println(" Thank you , Your Book is succesfully added");
       } else {
          System.out.println("Your Book is succesfully added");
          System.out.println("Please Enter the Valide Option : y or n");
              librarianAdminProcess();
       }
       }catch (java.lang.NullPointerException exception) {
         //exception.printStackTrace();
       } finally {
         //scanner.close();
       }
     }

     public Book getBookDetails() {
         Book book = new Book();
          //Scanner scanner = null;
          //scanner = new Scanner(System.in);
       try {
         System.out.println("Enter the Book Name :");
         String name = scanner.nextLine();
         BookServiceImplementation.getInstance().bookCheck(name);
         book.setName(name);
         System.out.println("Enter the AuthorName :");
         book.setAuthorName(scanner.nextLine());
         System.out.println("Enter the PublisherName :");
         book.setPublisherName(scanner.nextLine());
         System.out.println("Enter the PublisherContactNumber :");
         book.setPublisherContactNumber(scanner.nextLine());
          } catch(java.lang.NullPointerException exception) {
              //exception.printStackTrace();
          } finally {
            //scanner.close();
          }
         return book;
      }

    public void bookList() {
        List<Book> book = BookServiceImplementation.getInstance().bookList();
        System.out.println("Books");

        System.out.println("BookName \tAuthorName \t PublisherName \tPublisherContactNumber ");

        System.out.println(" ");

        if(book.isEmpty()) {

            System.out.println("No Book");

        } else {

            for(Book iteratebook:book) {

                 System.out.println(iteratebook.getName() + "\t" + iteratebook.getAuthorName() +"\t" + iteratebook.getPublisherName()+"\t"+iteratebook.getPublisherContactNumber());

            }

        }
    }

    public void deleteBook() {
           
       try {
           bookList();
            List<Book> book = BookServiceImplementation.getInstance().bookList();
             if(book.isEmpty()) {

                System.out.println("Book not is available ");

             } else {
                 System.out.println("Enter book Name :");

                 String bookName = scanner.nextLine();
                 BookServiceImplementation.getInstance().deleteBook(bookName);
                 System.out.println( "succesfully deleted");
           }
         } catch(java.lang.NullPointerException exception) {
              //exception.printStackTrace();
         } finally {
            //scanner.close();
         }            

      }
}
