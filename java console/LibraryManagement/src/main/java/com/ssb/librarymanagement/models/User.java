
package com.ssb.librarymanagement.models;


public class User {


    private int id;
    private String role;
    private String name;
    private String password;

    private String emailId;
    private String address;
    private String contactNumber;
    private int bookId;

 

   public int getId() {

        return id;

    }
    public void setId(int id) {

        this.id = id;

    }
    public int getBookId() {
         return bookId;
    }
    public void setBookId(int bookId) {
        this.bookId = bookId;
    }
    public String getRole(){
        return role;
    }
    public void setRole(String role){
       this.role = role;
    }
    public String getName() {

        return name;

    }



    public void setName(String name) {

        this.name = name;

    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
         this.password = password;
    }
    public String getEmailId() {
        return emailId;
    }
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    public String getAddress() {
        return address;
    }
     public void setAddress(String address) {
        this.address = address;
    }
    public String getContactNumber() {
        return contactNumber;
    }
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
   }
}