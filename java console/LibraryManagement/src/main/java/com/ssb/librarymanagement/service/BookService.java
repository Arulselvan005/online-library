
package com.ssb.librarymanagement.service;


import java.util.List;
import com.ssb.librarymanagement.models.Book;

public interface BookService {

     void addBook(Book book);

     List<Book> bookList();
     void bookCheck(String name);
     void deleteBook(String book);
}
