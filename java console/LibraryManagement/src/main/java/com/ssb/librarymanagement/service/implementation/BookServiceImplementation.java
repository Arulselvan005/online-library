
package com.ssb.librarymanagement.service.implementation;


import java.util.ArrayList;
import java.util.List;
import com.ssb.librarymanagement.models.Book;
import com.ssb.librarymanagement.service.BookService;
import com.ssb.librarymanagement.controller.LibrarianAdminController;

public class BookServiceImplementation implements BookService {
     public static List<Book> books = new ArrayList<>();
     public static BookService bookService;
     public static BookService getInstance() {
     if(bookService == null) {
         BookServiceImplementation bookService = new BookServiceImplementation();
         return bookService;
     }
     return bookService;
     }

 
     public void addBook(Book book) {
           books.add(book);
     }

     public List<Book>bookList() {
           return books;
     }

     public void deleteBook(String bookName) {
        for(Book iteratebook : books){
           if((iteratebook.getName()).equals(bookName)) {
                books.remove(iteratebook);
                break;
           }
        }
    }

     public void bookCheck(String name) {
         for(Book iteratebook:books) {
           if((iteratebook.getName()).equals(name)){
           System.out.println("This Book is Already Registered ,Try to another Book");
             LibrarianAdminController.getInstance().addBook();
             break;
          }
         }
     }
}



