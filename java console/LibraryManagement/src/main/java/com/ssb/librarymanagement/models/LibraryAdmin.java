
package com.ssb.librarymanagement.models;


public class LibraryAdmin {


    private int id;

    private String name;
    private String password;

    private int bookId;
    private int userId;

 

   public int getId() {

        return id;

    }
     public void setId(int id) {

        this.id = id;

    }
    public String getName() {

        return name;

    }



    public void setName(String name) {

        this.name = name;

    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
         this.password = password;
    }
    public int getBookId() {
         return bookId;
    }
    public void setBookId(int bookId) {
        this.bookId = bookId;
    }
    public int getUserId() {
        return userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
   }
}